import random

line = list(range(1, 16))
line += ['X']
# Перемешивание листа
random.shuffle(line)
win = list(range(1, 16))
win += ['X']
def get_new_game():
    global line
    game_line = []
    counter = 0
    for i in range(4):
        game_line.append([])
        for j in range(4):
            game_line[i].append(line[counter])
            counter += 1
    for values in game_line:
        print(values)

get_new_game()

def move_player():
    while True:
        text = input('Введите число от 1 до 15:')
        if text.isdigit() == False or 15 < int(text) or int(text) < 0:
            print('Вводите только целые числа от 1 до 15!')
        else:
            return int(text)
            False


while True:
    pusto = line.index('X')
    player = line.index(move_player())
    if pusto + 1 == player or pusto - 1 == player or pusto + 4 == player or pusto - 4 == player:
        line[pusto], line[player] = line[player], line[pusto]
        get_new_game()
    else:
        print('Такой ход не возможен')
    if line == win:
        print('Вы выиграли')
        False

