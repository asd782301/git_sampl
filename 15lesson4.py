list1 = list(map(int, input('Введите 1 список').split()))
list2 = list(map(int, input('Введите 2 список').split()))

def find_repeat(y):
    list3 = []
    for i in range (len(y)):
        x = y[i]
        if y.count(x) > 1 and (x not in list3):
            list3.append(x)    
    return list3
print(find_repeat(find_repeat(list1) + find_repeat(list2)))
