import sqlite3

con = sqlite3.connect('workers.db')

cur = con.cursor()
cur.execute('''CREATE TABLE workers(firstname text, lastname text, age int, special text, education text, experience int, wage int)''')

cur.execute("INSERT INTO workers VALUES('Иван','Иванов', 50, 'Токарь', 'Среднее', 10, 25000)")
cur.execute("INSERT INTO workers VALUES('Сергей','Сергеев',52, 'Электрик', 'Среднее', 15, 28000)")
cur.execute("INSERT INTO workers VALUES('Петр','Петров', 50, 'Бухгалтер', 'Высшее', 20, 35000)")
con.commit()

user = ('Василий', 'Пупкин', 45, 'Директор', 'Высшее', 20, 600000)
cur.execute("INSERT INTO workers VALUES(?, ?, ?, ?, ?, ?, ?);", user)
con.commit()

users = [('Алёна', 'Апина', 18, 'Стажер', 'Школа', 0, 6000), ('Альбина', 'Ахматова', 23, 'Литейщица', 'Высшее', 5, 60000)]
cur.executemany("INSERT INTO workers VALUES(?, ?, ?, ?, ?, ?, ?);", users)
con.commit()

for row in cur.execute('SELECT * FROM workers ORDER BY age'):
    print(row)
for row in cur.execute('SELECT * FROM workers ORDER BY experience'):
    print(row)
for row in cur.execute('SELECT * FROM workers ORDER BY wage'):
    print(row)

cur.execute("DELETE FROM workers WHERE firstname='Иван';")
con.commit()

cur.execute("SELECT * FROM workers")
print(cur.fetchmany(5))
con.close()