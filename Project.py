

class Car():

    def __init__(self, brand, model, color, price):
        self.color = color
        self.brand = brand
        self.model = model
        self.price = price

    def __str__(self):
        print(f'Машина {self.brand} {self.model}, цвет {self.color}, стоимостью {self.price}')

car1 = Car('BMW', 'x5', 'red', '1000$')
car2 = Car('BMW', 'x3', 'blue', '1100$')

class Menu(object):
   def __init__(self):
      self._options = {'a': self.optionA,
                       'b': self.optionB,
                       'c': self.optionC}

   def handle_options(self, option):
      if option not in self._options:
         print("Invalid option")

         return

      self._options[option]()

   def optionA(self):
      print("1.Купить машину.")

   def optionB(self):
      print("2.Продать машину.")

   def optionC(self):
       print("3.Показать все машины")

menu = Menu()
menu.optionA()
menu.optionB()
menu.optionC()


def choose(vibor):
    if vibor == '3':
        car2.__str__()
        car1.__str__()


choose(input('Выберите пункт '))
